<?php
require_once(__DIR__ . "/User.php");

class UserContainer {

    function __construct($db) {
        $this->users = [];
        $this->findAll($db);
    }

    function findAll($db) {

        $select = "SELECT id, name, district, active, last_notify_time, age, vaccine, dose FROM users";
        $stmt = $db->dbh->prepare($select);
        
        $stmt->execute();
        
        $this->users = [];
        while ($user = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $this->users[] = new User($user["id"], $user["name"], $user["district"], $user["active"], $user["last_notify_time"], $user["age"], $user["vaccine"], $user["dose"]);
        }

        return $this->users;

    }

    function getAll() {
        return $this->users;
    }

    function getAllActive($threshold_limit = 3600) {
        return array_filter($this->users, function($user) use($threshold_limit) {
            return $user->active && ($user->last_notify_time < ( (new DateTime())->getTimestamp()) - $threshold_limit);
        });
    }

    function getDistricts() {
        $needed_districts = array_map(function ($user) {
            return $user->district;
        }, $this->getAllActive());
        return array_unique($needed_districts);
    }

    function getDistrictWiseUsers() {
        $district_people = [];
        foreach ($this->getAllActive() as $user) {
            $district_people[$user->district][] = $user;
        }
        return $district_people;
    }


}