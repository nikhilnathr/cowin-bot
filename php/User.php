<?php
class User {

    function __construct($id = "", $name = "", $district = "", $active = 0, $last_notify_time = 0, $age = 120, $vaccine = null, $dose = 0) {
        $this->id = $id;
        $this->name = $name;
        $this->district = $district;
        $this->active = intval($active);
        $this->last_notify_time = intval($last_notify_time);
        $this->age = intval($age);
        $this->vaccine = $vaccine;
        $this->dose = intval($dose);   //  0 -> not set, 1 -> 1st dose, 2 -> 2nd dose
    }

    // update all other than username and password
    function update($db) {
        // Prepare UPDATE statement.
        $update = "UPDATE users SET name=:name, district=:district, active=:active, last_notify_time=:last_notify_time, age=:age, vaccine=:vaccine, dose=:dose WHERE id=:id";
        $stmt = $db->dbh->prepare($update);
        
        $stmt->bindParam(":id", $this->id, PDO::PARAM_STR);
        $stmt->bindParam(":name", $this->name, PDO::PARAM_STR);
        $stmt->bindParam(":district", $this->district, PDO::PARAM_STR);
        $stmt->bindParam(":active", $this->active, PDO::PARAM_INT);
        $stmt->bindParam(":last_notify_time", $this->last_notify_time, PDO::PARAM_STR);
        $stmt->bindParam(":age", $this->age, PDO::PARAM_INT);
        $stmt->bindParam(":vaccine", $this->vaccine, PDO::PARAM_STR);
        $stmt->bindParam(":dose", $this->dose, PDO::PARAM_INT);
        $result = $stmt->execute();
        
        if ($result === false) {
            throw new Exception("Couldn't update data");
            return false;
        }

        return true;
    }

    function find($db, $id) {
        // Prepare SELECT statement.
        $select = "SELECT id, name, district, active, last_notify_time, age, vaccine, dose FROM users WHERE id=?";
        $stmt = $db->dbh->prepare($select);

        if ($db->isError()) {
            throw new Exception($db->error);
        }
        
        // Execute statement.
        $stmt->execute([$id]);
        
        // Get the results.
        $results = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($results === false) {
            throw new Exception("Cannot find user {$id}");
            return false;
        }

        $this->id = $results["id"];
        $this->name = $results["name"];
        $this->district = $results["district"];
        $this->active = intval($results["active"]);
        $this->last_notify_time = intval($results["last_notify_time"]);
        $this->age = intval($results["age"]);
        $this->vaccine = $results["vaccine"];
        $this->dose = intval($results["dose"]);

        return true;
    }

    function save($db) {

        $update = "INSERT INTO users (id, name, district, active, age, vaccine, dose) VALUES(:id, :name, :district, :active, :age, :vaccine, :dose)";
        $stmt = $db->dbh->prepare($update);
        
        $stmt->bindParam(":id", $this->id, PDO::PARAM_STR);
        $stmt->bindParam(":name", $this->name, PDO::PARAM_STR);
        $stmt->bindParam(":district", $this->district, PDO::PARAM_STR);
        $stmt->bindParam(":active", $this->active, PDO::PARAM_INT);
        $stmt->bindParam(":age", $this->age, PDO::PARAM_INT);
        $stmt->bindParam(":vaccine", $this->vaccine, PDO::PARAM_STR);
        $stmt->bindParam(":dose", $this->dose, PDO::PARAM_INT);
        $stmt->execute();

        if ($db->isError() !== false) {
            throw new Exception("Couldn\"t create user");
        }
    }
}