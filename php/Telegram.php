<?php
class Telegram {

    function __construct($token, $commands = []) {
        $this->token = $token;
        $this->commands = $commands;
    }

    function sendMessage($chatId, $text, $options = []) {
        $text = $this->replaceSpecialCharacters($text);
        $textChunks = str_split($text, 4000);
        $chunksCount = count($textChunks);
        
        if ($chunksCount > 1) {
            for ($i=0; $i < $chunksCount - 1; $i++) {
                $extra = [
                    "chat_id" => $chatId,
                    "text" => $textChunks[$i],
                    "parse_mode" => isset($options["parse_mode"])? $options["parse_mode"] : "MarkdownV2",
                    "disable_web_page_preview" => isset($options["disable_web_page_preview"])? $options["disable_web_page_preview"] : false,
                ];
                send_post("https://api.telegram.org/bot{$this->token}/sendMessage", $extra);
            }
        }
        $extra = [
            "chat_id" => $chatId,
            "text" => $textChunks[$chunksCount - 1],
            "parse_mode" => isset($options["parse_mode"])? $options["parse_mode"] : "MarkdownV2",
            "disable_web_page_preview" => isset($options["disable_web_page_preview"])? $options["disable_web_page_preview"] : false,
            "reply_markup" => isset($options["reply_markup"])? json_encode($options["reply_markup"]) : []
        ];

        return send_post("https://api.telegram.org/bot{$this->token}/sendMessage", $extra);
    }

    function editMessageText($chatId, $messageId, $text, $options = []) {
        $edit_message_repeat_error = "specified new message content and reply markup are exactly the same as a current content and reply markup of the message";

        $text = $this->replaceSpecialCharacters($text);
        $extra = [
            "chat_id" => $chatId,
            "message_id" => $messageId,
            "text" => $text,
            "parse_mode" => isset($options["parse_mode"])? $options["parse_mode"] : "MarkdownV2",
            "disable_web_page_preview" => isset($options["disable_web_page_preview"])? $options["disable_web_page_preview"] : false,
            "reply_markup" => isset($options["reply_markup"])? json_encode($options["reply_markup"]) : []
        ];

        try {
            return send_post("https://api.telegram.org/bot{$this->token}/editMessageText", $extra);
        } catch (Exception $e) {
            if (strpos($e->getMessage(), $edit_message_repeat_error) === false) {
                throw $e;
            }
        }
    }

    function setWebhook($url) {
        send_post("https://api.telegram.org/bot{$this->token}/setWebhook", [
            "url" => $url
        ]);
    }

    function recieveAndHandleMessage() {
        $inputJSON = file_get_contents('php://input');
        $input = json_decode($inputJSON);

        if (isset($input->callback_query)) {
            $chatId = $input->callback_query->from->id;
            $chatText = $input->callback_query->data;

            if ($this->executeCommandsFromCallbackQuery($input->callback_query, $chatId, $chatText) === false) {
                $this->sendGenericResponse($chatId);
            }
        } else if (isset($input->message)) {
            $chatId = $input->message->chat->id;
            $chatText = $input->message->text;
    
            if ($this->executeCommandFromMessage($input->message, $chatId, $chatText) === false) {
                $this->sendGenericResponse($chatId);
            }
        }
    }

    function executeCommandIfExists($text, $chatId, $fromUser, $options=[]) {
        $arrays = explode(" ", trim($text));
        $commandName = $arrays[0];
        $arguments = array_slice($arrays, 1);

        $options["name"] = $fromUser->username;
        $options["first_name"] = $fromUser->first_name;
        $options["last_name"] = $fromUser->last_name;
        $options["chat_id"] = $chatId;

        if (array_key_exists($commandName, $this->commands)) {
            $commandFunction = $this->commands[$commandName]["function"];
            $commandFunction($chatId, $arguments, $options, $this);
            return true;
        } else {
            $this->sendMessage($chatId, "Command not recognised");
            $this->sendGenericResponse($chatId);
        }
    }

    function executeCommandsFromCallbackQuery($callback_query, $chatId, $chatText) {
        $fromUser = $callback_query->from;

        $options = [];
        $options["message_id"] = $callback_query->message->message_id;

        return $this->executeCommandIfExists($chatText, $chatId, $fromUser, $options);
    }

    function executeCommandFromMessage($message, $chatId, $chatText) {

        if (@isset($message->entities)) {
            foreach ($message->entities as $entity) {
                if ($entity->type === "bot_command") {

                    $commandText = substr($chatText, $entity->offset);

                    $fromUser = $message->from;
                    
                    // Only one command per message is executed
                    return $this->executeCommandIfExists($commandText, $chatId, $fromUser);
                }
            }
        }

        return false;
    }

    function sendGenericResponse($chatId) {
        $helpString = "";
        foreach ($this->commands as $command=>$config) {
            $helpString .= "\n{$command} - {$config["description"]}";
        }
        $this->sendMessage($chatId, "Available commands are: ". $helpString);
    }

    function replaceSpecialCharacters($text) {
        $text = str_replace(".", "\\.", $text);
        $text = str_replace("=", "\\=", $text);
        $text = str_replace("_", "\\_", $text);
        $text = str_replace("-", "\\-", $text);
        $text = str_replace("(", "\\(", $text);
        $text = str_replace(")", "\\)", $text);
        $text = str_replace("!", "\\!", $text);
        $text = str_replace("|", "\\|", $text);
        $text = str_replace("<", "\\<", $text);
        $text = str_replace(">", "\\>", $text);
        $text = str_replace("*", "\\*", $text);
        $text = str_replace("[", "\\[", $text);
        $text = str_replace("]", "\\]", $text);
        $text = str_replace("~", "\\~", $text);
        $text = str_replace("#", "\\#", $text);
        $text = str_replace("+", "\\+", $text);
        $text = str_replace("{", "\\{", $text);
        $text = str_replace("}", "\\}", $text);
        return $text;
    }

}

