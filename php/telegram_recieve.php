<?php
require_once(__DIR__ ."/php/helpers.php");
require_once(__DIR__ ."/php/Config.php");
require_once(__DIR__ ."/php/Cowin.php");
require_once(__DIR__ ."/php/Database.php");
require_once(__DIR__ ."/php/User.php");
require_once(__DIR__ ."/php/Telegram.php");

$config = new Config();

$startCommand = function ($chatId, $arguments, $options, $telegram) {
    $config = new Config();
    $db = new Database($config->DBPATH);
    $db->connect($config->DBUSERNAME, $config->DBPASSWORD);

    try {
        $user = new User();
        $user->find($db, $chatId);

        if ($user->active) {
            $telegram->sendMessage($chatId, "✅ You are already registered and notifications are active\n/showstates - change your district");
        } else {
            $telegram->sendMessage($chatId, "⛔ You are registered but notifications are inactive\n/activate - activate notifications");
        }
    } catch (Exception $e) {
        $telegram->sendMessage(
            $chatId,
            "This is not an official bot and is not related to the government. This bot just provides vaccine information. For more info https://gitlab.com/nikhilnathr/cowin-bot.",
            [
                "disable_web_page_preview" => true
            ]);
        $telegram->sendMessage($chatId, "❕ You are not registered. \nStart by the /showstates command");
    }
};

$showStatesCommand = function ($chatId, $arguments, $options, $telegram) {
    $cowin = new Cowin();
    $states = $cowin->getAllStates();

    $message = [];
    $i = 0;
    $counter = 0;
    foreach ($states as $state) {
        $message[$counter][] = ["text" => "$state->state_name", "callback_data" => "/showdistricts $state->state_id"];
        $i++;
        if ($i % 3 == 0) {
            $counter++;
        }
    }

    $telegram->sendMessage($chatId, "Select a state", 
        [
            "reply_markup" => [
                "inline_keyboard" => $message
            ]
        ]
    );
};

$showDistrictsCommand = function ($chatId, $arguments, $options, $telegram) {
    $config = new Config();
    $cowin = new Cowin();

    if (count($arguments) < 1) {
        $telegram->sendMessage($chatId, "Invalid usage: /showdistricts <state_id>");
        return false;
    }

    $districts = $cowin->getAllDistricts($arguments[0]);

    $message = [];
    $i = 0;
    $counter = 0;
    foreach ($districts as $district) {
        $message[$counter][] = ["text" => "$district->district_name", "callback_data" => "/setdistrict $district->district_id"];
        $i++;
        if ($i % 3 == 0) {
            $counter++;
        }
    }

    if (isset($options["chat_id"]) && isset($options["message_id"])) {
        $telegram->editMessageText($options["chat_id"], $options["message_id"], "Select a district",  [ "reply_markup" => [ "inline_keyboard" => $message ]]);
    } else {
        $telegram->sendMessage($chatId, "Select a district", [ "reply_markup" => [ "inline_keyboard" => $message ]]);
    }

};

$setDistrictCommand = function ($chatId, $arguments, $options, $telegram) {
    $config = new Config();
    $db = new Database($config->DBPATH);
    $db->connect($config->DBUSERNAME, $config->DBPASSWORD);

    if (count($arguments) < 1) {
        $telegram->sendMessage($chatId, "Invalid usage: /setdistrict <district_id>");
        return false;
    }

    $user = new User();
    try {
        $user->find($db, $chatId);
        $user->id = $chatId;
        $user->name = $options["name"];
        $user->district = $arguments[0];
        $user->active = 1;
        
        $user->update($db);
    } catch (Exception $e) {
        $user->id = $chatId;
        $user->name = $options["name"];
        $user->district = $arguments[0];
        $user->active = 1;
        
        $user->save($db);
    }

    $message = [
        "✅ Notifications are active. You will recieve message when vaccine is available.",
        "/setage <age> - set your age for vaccine",
        "/showdose - select a your dose number",
        "/showvaccines - select a specific vaccine type",
        "/showstates - change districts",
        "/deactivate - deactivate notifications"
    ];
    $message = implode("\n", $message);
    if (isset($options["chat_id"]) && isset($options["message_id"])) {
        $telegram->editMessageText($options["chat_id"], $options["message_id"], $message);
    } else {
        $telegram->sendMessage($chatId, $message);
    }


};

$activateCommand = function ($chatId, $arguments, $options, $telegram) {

    $config = new Config();
    $db = new Database($config->DBPATH);
    $db->connect($config->DBUSERNAME, $config->DBPASSWORD);

    $user = new User();
    try {
        $user->find($db, $chatId);
        $user->active = 1;
        
        $user->update($db);
        $telegram->sendMessage($chatId, "✅ Notifications are set to active.");
    } catch (Exception $e) {
        $telegram->sendMessage($chatId, "Please select a district first. Search for districts using /showstates command.");
    }

};

$deactivateCommand = function ($chatId, $arguments, $options, $telegram) {

    $config = new Config();
    $db = new Database($config->DBPATH);
    $db->connect($config->DBUSERNAME, $config->DBPASSWORD);

    $user = new User();
    try {
        $user->find($db, $chatId);
        $user->active = 0;
        
        $user->update($db);
        $telegram->sendMessage($chatId, "⛔ Notifications are have been deactivated!\n/activate - activate notifications");
    } catch (Exception $e) {
        $telegram->sendMessage($chatId, "Please select a district first. Search for districts using /showstates command.");
    }

};

$resetThresholdCommand = function ($chatId, $arguments, $options, $telegram) {

    $config = new Config();
    $db = new Database($config->DBPATH);
    $db->connect($config->DBUSERNAME, $config->DBPASSWORD);

    $user = new User();
    try {
        $user->find($db, $chatId);
        $user->last_notify_time = 0;
        
        $user->update($db);
        $telegram->sendMessage($chatId, "Notification threshold limit reset.");
    } catch (Exception $e) {
        $telegram->sendMessage($chatId, "Please select a district first. Search for districts using /showstates command.");
    }

};

$setAgeCommand = function ($chatId, $arguments, $options, $telegram) {

    $config = new Config();
    $db = new Database($config->DBPATH);
    $db->connect($config->DBUSERNAME, $config->DBPASSWORD);

    if (count($arguments) < 1) {
        $telegram->sendMessage($chatId, "Invalid usage: /setage <age>");
        return false;
    }
    
    $age = intval($arguments[0]);
    $lowerLimitAge = 10;
    $upperLimitAge = 150;
    if ($age < $lowerLimitAge || $age > $upperLimitAge) {
        $telegram->sendMessage($chatId, "Invalid age ({$lowerLimitAge} < age < {$upperLimitAge})");
        return false;
    } 

    $user = new User();
    try {
        $user->find($db, $chatId);
        $user->age = $age;
        
        $user->update($db);
        $telegram->sendMessage($chatId, "Your age limit for notifications is set as {$age}\nYou can reset the age by '/setage 120'");
    } catch (Exception $e) {
        $telegram->sendMessage($chatId, "Please select a district first. Search for districts using /showstates command.");
    }

};

$showVaccinesCommand = function ($chatId, $arguments, $options, $telegram) {

    $setVaccineCommandLabel = '/setvaccine';
    $vaccines = [
        "Covaxin"       => "COVAXIN",
        "Covishield"    => "COVISHIELD",
        "No preference" => ""
    ];

    $i = 0;
    $j = 0;
    foreach ($vaccines as $name=>$id) {
        $message[$i][$j] = ["text" => "{$name}", "callback_data" => "{$setVaccineCommandLabel} {$id}"];
        $j++;
        if ($j % 2 == 0) {
            $i++;
            $j = 0;
        }
    }

    $telegram->sendMessage($chatId, "Select a vaccine", 
        [
            "reply_markup" => [
                "inline_keyboard" => $message
            ]
        ]
    );
};

$setVaccineCommand = function ($chatId, $arguments, $options, $telegram) {

    $config = new Config();
    $db = new Database($config->DBPATH);
    $db->connect($config->DBUSERNAME, $config->DBPASSWORD);

    if (count($arguments) > 1) {
        $telegram->sendMessage($chatId, "Invalid usage: /setvaccine [vaccine]");
        return false;
    }
    
    $vaccine = null;
    if (count($arguments) === 1) {
        if (array_search(strtoupper($arguments[0]), ["COVAXIN", "COVISHIELD"]) !== false) {
            $vaccine = strtoupper($arguments[0]);
        } else {
            $telegram->sendMessage($chatId, "Vaccine name is invalid");
            return false;
        }
    }

    $user = new User();
    try {
        $user->find($db, $chatId);
        $user->vaccine = $vaccine;
        
        $user->update($db);

        $vaccineType = "set as {$vaccine}";
        if (is_null($vaccine)) {
            $vaccineType = "reset";
        }
        $reply = "Your vaccine type is {$vaccineType}\n/showvaccines - change vaccine type again";
        if (isset($options["chat_id"]) && isset($options["message_id"])) {
            $telegram->editMessageText($options["chat_id"], $options["message_id"], $reply);
        } else {
            $telegram->sendMessage($chatId, $reply);
        }
    } catch (Exception $e) {
        $telegram->sendMessage($chatId, "Please select a district first. Search for districts using /showstates command.");
    }

};

$suggestionCommand = function ($chatId, $arguments, $options, $telegram) {

    $config = new Config();

    if (count($arguments) < 1) {
        $telegram->sendMessage($chatId, "Invalid usage: /suggestion <suggestion>");
        return false;
    }

    $review = "{$chatId} [{$options['name']}, {$options['first_name']} {$options['last_name']}]: " . implode(" ", $arguments);
    file_put_contents(__DIR__ . "/php/suggestion.log", json_encode($review) . PHP_EOL, FILE_APPEND);

    $telegram->sendMessage($chatId, "Thank you for your valuable feedback");
    $telegram->sendMessage($config->TELEGRAM_ADMIN, "New Review: {$review}");
};

$setDoseCommand = function ($chatId, $arguments, $options, $telegram) {
    $config = new Config();
    $db = new Database($config->DBPATH);
    $db->connect($config->DBUSERNAME, $config->DBPASSWORD);

    if (count($arguments) > 1) {
        $telegram->sendMessage($chatId, "Invalid usage: /setdose [dose_no]\n0 - No preference\n1 - 1st dose\n2 - 2nd dose");
        return false;
    }
    
    $dose = 0;
    if (count($arguments) === 1) {
        if (array_search(strtoupper($arguments[0]), ["0", "1", "2"]) !== false) {
            $dose = intval($arguments[0]);
        } else {
            $telegram->sendMessage($chatId, "Dose number is invalid");
            return false;
        }
    }

    $user = new User();
    try {
        $user->find($db, $chatId);
        $user->dose = $dose;
        
        $user->update($db);

        $vaccineType = "set to dose - {$dose}";
        if ($dose === 0) {
            $vaccineType = "reset";
        }
        $reply = "Your dose number is {$vaccineType}\n/showdose - change dose number again";
        if (isset($options["chat_id"]) && isset($options["message_id"])) {
            $telegram->editMessageText($options["chat_id"], $options["message_id"], $reply);
        } else {
            $telegram->sendMessage($chatId, $reply);
        }
    } catch (Exception $e) {
        $telegram->sendMessage($chatId, "Please select a district first. Search for districts using /showstates command.");
    }
};

$showDoseCommand = function ($chatId, $arguments, $options, $telegram) {

    $setDoseCommandLabel = '/setdose';
    $doses = [
        "Dose - 1"      => "1",
        "Dose - 2"      => "2",
        "No preference" => "0"
    ];

    $i = 0;
    $j = 0;
    foreach ($doses as $name=>$id) {
        $message[$i][$j] = ["text" => "{$name}", "callback_data" => "{$setDoseCommandLabel} {$id}"];
        $j++;
        if ($j % 2 == 0) {
            $i++;
            $j = 0;
        }
    }

    $telegram->sendMessage($chatId, "Select a dose number", 
        [
            "reply_markup" => [
                "inline_keyboard" => $message
            ]
        ]
    );
};

$commands = [
    "/start" => [
        "name" => "Start",
        "description" => "Welcome message or show your status",
        "function" => $startCommand,
    ],
    "/showstates" => [
        "name" => "Show states",
        "description" => "Show available states in cowin app",
        "function" => $showStatesCommand,
    ],
    "/showdistricts" => [
        "name" => "Show districts",
        "description" => "Show available districts from a state in cowin app",
        "function" => $showDistrictsCommand,
    ],
    "/setdistrict" => [
        "name" => "Set a district",
        "description" => "Set a district to show the notifications",
        "function" => $setDistrictCommand,
    ],
    "/activate" => [
        "name" => "Activate notifications",
        "description" => "Activate notifications for you",
        "function" => $activateCommand,
    ],
    "/deactivate" => [
        "name" => "Deactivate notifications",
        "description" => "Deactivate all notifications from the bot",
        "function" => $deactivateCommand,
    ],
    "/reset" => [
        "name" => "Reset notification threshold",
        "description" => "Remove the timer delay after receiving a notification",
        "function" => $resetThresholdCommand,
    ],
    "/setage" => [
        "name" => "Set age limit",
        "description" => "Set the upper age limit for notifications",
        "function" => $setAgeCommand,
    ],
    '/showvaccines' => [
        "name" => "Show vaccines",
        "description" => "Show available vaccines",
        "function" => $showVaccinesCommand,
    ],
    '/setvaccine' => [
        "name" => "Set a vaccines",
        "description" => "Only show a specific vaccine notifications",
        "function" => $setVaccineCommand,
    ],
    '/suggestion' => [
        "name" => "Add your suggestions",
        "description" => "Only show a specific vaccine notifications",
        "function" => $suggestionCommand,
    ],
    '/showdose' => [
        "name" => "Show dose",
        "description" => "Show available dose numbers",
        "function" => $showDoseCommand,
    ],
    '/setdose' => [
        "name" => "Add your dose number",
        "description" => "Set the dose number you want notifications for.",
        "function" => $setDoseCommand,
    ]
];


$telegram = new Telegram($config->TELEGRAM_TOKEN, $commands);

$data = [
    "raw_post" => file_get_contents('php://input'),
    "time" => (new DateTime())->getTimestamp(),
];

try {
    
    $telegram->recieveAndHandleMessage();
    
} catch (Exception $e) {

    $current_timestamp = (new DateTime())->getTimestamp();
    $telegram->sendMessage($config->TELEGRAM_ADMIN, "Some error happened when processing a message [{$current_timestamp}]");

    $data["message"] = $e->getMessage();
    $data["stacktrace"] = $e->getTraceAsString();

}
file_put_contents(__DIR__ . "/php/telegram.log", json_encode($data) . PHP_EOL, FILE_APPEND);