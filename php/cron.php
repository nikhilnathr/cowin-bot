<?php
require_once(__DIR__ ."/helpers.php");
require_once(__DIR__ ."/Config.php");
require_once(__DIR__ ."/Cowin.php");
require_once(__DIR__ ."/Database.php");
require_once(__DIR__ ."/Telegram.php");
require_once(__DIR__ ."/User.php");
require_once(__DIR__ ."/UserContainer.php");

$url = "https://cdn-api.co-vin.in/api/v2/appointment/sessions/public/calendarByDistrict";
$weeks_check_limit = 2; // check for 2 consecutive weeks
$current_timestamp = (new DateTime())->getTimestamp();

$config = new Config();
$db = new Database($config->DBPATH);
$telegram_bot = new Telegram($config->TELEGRAM_TOKEN);
$cowin = new Cowin();

echo "[{$current_timestamp}]\n";
$send_notifications_count = 0;
try {
    $db->connect($config->DBUSERNAME, $config->DBPASSWORD);
    
    $users_container = new UserContainer($db);

    $needed_districts = $users_container->getDistricts();
    $district_people = $users_container->getDistrictWiseUsers();
    
    foreach ($needed_districts as $district) {
        
        echo "Searching for district '{$district}' for {$weeks_check_limit} weeks.\n";

        $vaccine_data = $cowin->getActiveVaccinationCenters($district, $current_timestamp, $weeks_check_limit);

        echo count($vaccine_data) ." vaccines found in '{$district}'\n";
        if (count($vaccine_data) > 0) {

            foreach ($district_people[$district] as $subscriber) {
                
                // After 20 notifications sleep to try to avoid telegram rate limit
                if (($send_notifications_count + 1) % 20  === 0) {
                    echo "Sleeping for rate limit";
                    sleep(5);
                }
                
                // Filter according to the user preference (age and vaccine)
                $filtered_vaccine_data = array_filter($vaccine_data, function($data) use($subscriber) {
                    if ($data->age <= $subscriber->age) {
                        if (is_null($subscriber->vaccine) || strtoupper($data->vaccine) === $subscriber->vaccine) {
                            if ($subscriber->dose === 0 || $data->dose->{$subscriber->dose} > 0) {
                                return true;
                            }
                        }
                    }

                    return false;
                });

                if (count($filtered_vaccine_data) < 1) {
                    continue;
                }

                
                // Construct the message 
                $message_text = "Vaccine avaialable:\n";
                $i = 1;
                foreach ($filtered_vaccine_data as $data) {
                    $message_text .= "{$i}) $data->center ({$data->vaccine})\n    at {$data->date} | {$data->capacity} nos ({$data->dose->{1}}, {$data->dose->{2}}) | {$data->age} age \n";
                    $i++;
                }
                // Truncate long messages
                if (strlen($message_text) > 500) {
                    $message_text = substr($message_text, 0, 500);
                    $message_text .= "\n...and many more(". count($filtered_vaccine_data) ." centers in total).\n";
                }
                $message_text .= "\nAfter this notification there will be a cooldown for 1 hour. Look into the bot's usage on how to disable it.\n\nBook at https://selfregistration.cowin.gov.in/\n";
                    
                // Send message and activate cooldown
                echo "Send to {$subscriber->name}\n";
                try {
                    $telegram_bot->sendMessage($subscriber->id, $message_text);
                    $subscriber->last_notify_time = $current_timestamp;
                    $subscriber->update($db);
                } catch (Exception $e) {
                    echo "Error: {$e->getMessage()}\n";
                    $telegram_bot->sendMessage($config->TELEGRAM_ADMIN, "Error in sending message in cron");
                    $telegram_bot->sendMessage($config->TELEGRAM_ADMIN, "{$e->getMessage()}");
                }
            }

        }

    }

} catch (Exception $e) {
    echo "Error: {$e->getMessage()}\n";

    if (!is_null($config->TELEGRAM_ADMIN)) {
        $telegram_bot->sendMessage($config->TELEGRAM_ADMIN, "Cron job stopped working");
        $telegram_bot->sendMessage($config->TELEGRAM_ADMIN, "{$e->getMessage()}");
        $telegram_bot->sendMessage($config->TELEGRAM_ADMIN, "{$e->getTraceAsString()}");
    }

    exit(1);
}

