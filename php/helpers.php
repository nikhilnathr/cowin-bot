<?php


function send_get($url, $data = array(), $headers = []) {
    $data_part = (!empty($data)) ? "?" . http_build_query($data) : "";
    $url .= (substr($url, -1) === "/") ? "" : "/";
    $url .= $data_part;
    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    // curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
    // curl_setopt($ch, CURLOPT_HEADER, 1);
    
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    $server_output = curl_exec($ch);
    $response_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);

    if ($server_output === false) {
        throw new Exception("Request was not successful ('{$url}')");
    } else if (!($response_code >= 200 && $response_code < 300)) {
        throw new Exception("Error code {$response_code} ('{$url}'): {$server_output}");
    }

    return $server_output;
}

function send_post($url, $data = array(), $headers = [], $raw_data = null) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, 1);
    if (is_null($raw_data)) {
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
    } else {
        curl_setopt($ch, CURLOPT_POSTFIELDS, $raw_data);
    }
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    $server_output = curl_exec($ch);
    $response_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);

    if ($server_output === false) {
        throw new Exception("Request was not successful ('{$url}')");
    } else if (!($response_code >= 200 && $response_code < 300)) {
        throw new Exception("Error code {$response_code} ('{$url}'): {$server_output}");
    }

    return $server_output;
}

