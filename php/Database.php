<?php
class Database {
    public $error = null;
    public $errorCode = null;

    function __construct($path = null) {
        if (is_null($path)) {
            $path = "sqlite:" . __DIR__ . "/../../users.sqlite";
        }
        $this->path = $path;
    }
    
    function connect($username = null, $password=null) {
        if (!is_null($username) && !is_null($password)) {
            $this->dbh = new PDO($this->path, $username, $password);
        } else {
            $this->dbh  = new PDO($this->path) or die("cannot open the database");
        }
        return $this->dbh;
    }

    function create_tables() {
        // Create users table.
        $users_table_query = "CREATE TABLE IF NOT EXISTS users (
            id VARCHAR(100) NOT NULL PRIMARY KEY,
            name VARCHAR(255) NULL,
            district VARCHAR(255) NULL,
            active INTEGER NOT NULL DEFAULT 0,
            last_notify_time VARCHAR(25) NOT NULL DEFAULT '0',
            age INTEGER NOT NULL DEFAULT 120,
            vaccine VARCHAR(25) NULL,
            dose INTEGER DEFAULT 0
            )";
        $this->dbh->exec($users_table_query);
        
        if ($this->isError() !== false) {
            throw new Exception("Couldn\"t create table: {$this->error}");
        }
    }

    function run_migrations() {
        // Add age column (Migration from: f90a37c50b54)
        $age_query = "ALTER TABLE users ADD COLUMN age INTEGER NOT NULL DEFAULT 120 AFTER last_notify_time";
        
        $this->exec($age_query);
        
        // Neglect duplicate column error (means already the column exists)
        $sql_duplicate_column_error = "42S21";
        if ($this->isError() !== false && $this->errorCode !== $sql_duplicate_column_error) {
            throw new Exception("Migration have not been applied (f90a37c50b54) - {$this->error}");
        } else if ($this->errorCode !== $sql_duplicate_column_error) {
            echo "Applied migrations from: f90a37c50b54\n";
        }

        // Add vaccine column (Migration from: c5296e7d758e)
        $vaccine_query = "ALTER TABLE users ADD COLUMN vaccine VARCHAR(25) NULL AFTER age";

        $this->exec($vaccine_query);
        
        // Neglect duplicate column error (means already the column exists)
        if ($this->isError() !== false && $this->errorCode !== $sql_duplicate_column_error) {
            throw new Exception("Migration have not been applied (c5296e7d758e) - {$this->error}");
        } else if ($this->errorCode !== $sql_duplicate_column_error) {
            echo "Applied migrations from: c5296e7d758e\n";
        }

        // Add optional dose count (Migration from: 29ae2fd86261)
        $dose_query = "ALTER TABLE users ADD COLUMN dose INTEGER DEFAULT 0 AFTER vaccine";

        $this->exec($dose_query);
        
        // Neglect duplicate column error (means already the column exists)
        if ($this->isError() !== false && $this->errorCode !== $sql_duplicate_column_error) {
            throw new Exception("Migration have not been applied (29ae2fd86261) - {$this->error}");
        } else if ($this->errorCode !== $sql_duplicate_column_error) {
            echo "Applied migrations from: 29ae2fd86261\n";
        }
    }

    function exec($query) {
        $this->error = null;
        $this->errorCode = null;

        return $this->dbh->exec($query);
    }
    
    function isError() {
        if (is_null($this->dbh->errorInfo()[1])) {
            return false;
        } 
        $this->error = $this->dbh->errorInfo()[2];
        $this->errorCode = $this->dbh->errorInfo()[0];
        return true;
    }

    function getTotalCount($table_name, $suffix = "") {
        $sql = "SELECT COUNT(*) FROM $table_name $suffix"; 
        $result = $this->dbh->prepare($sql); 
        $result->execute(); 
        return $result->fetchColumn(); 
    }
}
