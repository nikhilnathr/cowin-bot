<?php
  class Config {
        
    function __construct($path = __DIR__ . "/config.json") {
      $this->getConfigFromEnv = false;
      if (file_exists($path)) {
        $keys = json_decode(file_get_contents($path), true);
        if (!is_null($keys)) {
            foreach ($keys as $key => $value) {
                putenv("{$key}=${value}");
            }
        }
      }
    }
    
    public function __get($property) {
      if (getenv($property) !== false) {
        return getenv($property);
      } else {
        return null;
      }
    } 
  }
