<?php

require_once(__DIR__."/helpers.php");

class Cowin {
    private $user_agent = "User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.89 Safari/537.36";
    private $base_url = "https://cdn-api.co-vin.in/api";

    function getActiveVaccinationCenters($district, $startTimestamp, $checkWeeks) {
        $success_data = [];
        for ($i = 0; $i < $checkWeeks; $i++) {
            $date = date('d-m-Y', $startTimestamp + $i * 7 * 24 * 60 * 60);
            $data = [ "district_id" => $district, "date" => $date];
    
            $cowin = json_decode(send_get("{$this->base_url}/v2/appointment/sessions/public/calendarByDistrict", $data, [$this->user_agent]), false);
    
            foreach ($cowin->centers as $center) {
                foreach ($center->sessions as $session) {
                    if ($session->available_capacity > 0) {
                        // $message_text .= "\n{$center->name} on {$session->date}, $session->available_capacity numbers (Age limit: {$session->min_age_limit})";
                        $success_data[] = [ 
                            "center" => $center->name,
                            "date" => $session->date,
                            "capacity" => $session->available_capacity,
                            "dose" => [
                                1 => $session->available_capacity_dose1,
                                2 => $session->available_capacity_dose2
                            ],
                            "fee" => $center->fee_type,
                            "age" => $session->min_age_limit,
                            "vaccine" => $session->vaccine,
                        ];
                    }
                }
            }   
        }

        return json_decode(json_encode($success_data));
    }

    function getAllStates() {
        $states = json_decode(send_get("{$this->base_url}/v2/admin/location/states", [], [$this->user_agent]), false);
        return $states->states;
    }

    function getAllDistricts($stateId) {
        $districts = json_decode(send_get("{$this->base_url}/v2/admin/location/districts/$stateId", [], [$this->user_agent]), false);
        return $districts->districts;
    }
}