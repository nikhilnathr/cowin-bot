
# Cowin Notification Bot

  

Get [Telegram](https://telegram.org/) notifications of new and available Covid-19 vaccines in India from the [official cowin](https://cowin.gov.in) website. This project is not affiliated in anyway with the Government of India or the cowin website. It just uses their API for sending out notifications when vaccinations are available. 

Please do not rely wholly on this bot for registering the vaccine, you should also check the website periodically too as this bot is not in anyway responsible for any problems related to the booking of vaccines.

**This is a work in progress so you may encounter some bugs and there may be quick changes to the interface.**

There would be a limit in number of simultaneous requests to telegram. So, I encourage you to setup your own instance of this notification bot for you and your friends and family. For now this bot does not have any specific limits but this may change in the near future.

## Usage Instructions


Available bot commandsare,

| Command          | Parameters          | Description
|------------------|---------------------|-----------------------|
| `/start` 	   | - 			    | Welcome message or show your status |
| `/showstates`    | - 			    | Show available states in cowin app |
| `/showdistricts` | state_id (*int*) 	    | Show available districts from a state in cowin app |
| `/setdistrict`   | district_id (*int*)    | Set a district to show the notifications |
| `/activate` 	   | - 			    | Activate the notifications |
| `/deactivate`    | - 			    | Deactivate all notifications from the bot |
| `/reset` 	   | - 			    | Remove the timer delay after receiving a notification |
| `/setage` 	   | \*age (*int*)          | Set the upper age limit for notifications |
| `/showvaccines`  | -		            | Show and available vaccines |
| `/setvaccine`    | vaccine_name(*string*) | Set the notification vaccine type. If there is no vaccine name the type in reset |
| `/suggestion`    | \*suggestion(*string*) |Add your suggestions or review for this bot
 \* marked parameters are required
 
## Setup Instructions

You should have some knowledge in setting up web related software before setting up. For setting up this project and receiving telegram messages you should have a public IP.
This project was made with the Linux OSs in mind as most of the servers are Linux based. So other operating systems may have to deviate from these instructions.

**Requires**  

- PHP >= `7` (Tested only in `7.2.24` and `7.4.3` others should also work).
- curl
- php-curl
- mysql/postgres database
- A web server

### Clone the repository

Clone the project repository (preferably in the web root `/var/www`).
```sh
git clone https://gitlab.com/nikhilnathr/cowin-bot.git
cd cowin-bot
```

### Setup a SQL database

Create a SQL database and a user with all permissions to the database.
  
### Copy and fill configuration

Copy the sample configuration file from the `/php/config.sample.json` to `/php/config.json` and fill in the correct values.

```sh
cp php/config.sample.json php/config.json
```
- `TELEGRAM_TOKEN`: The Telegram token got from the [@botfather](https://t.me/botfather)
- `TELEGRAM_ADMIN`: The id of the admin user (most probably you). This is used to send notifications in case of errors.
- `SERVER_URL`: The base URL of your server. Should be a **https** URL.


### Run the initialization script

Run the initialization script, which checks for the configuration variables, SQL connection, creates a tables and creates a web-hook receive for the telegram bot.

```
php php/init.php
```  

>  **Important:** This initialization script should be run after each update of the software.

### Set the cronjob

The line to setup cronjob would be shown at the last line of initialization script. (You can run it again if you closed the previous window)

Cronjob is responsible for checking and sending the notification every 5 minutes.
```sh
crontab -e
```

### [*Very Important*] Hide `/php` folder and setup webserver

In the configuration of your server hide the `/php` folder from public access. Hiding the `/php` folder is very important because it contains all your configuration.

Set up a block in your server which will serve the files in the root folder of your project. (Other than `/php`).

A sample `nginx` configuration would look like:
```nginx
server {
        listen 80;
        listen [::]:80;

        server_name mydomain.com;

        root /var/www/cowin-bot;
        index index.html index.php;
  
        location / {
                try_files $uri $uri/ /index.php$is_args$args;
        }

        location ~ \.php$ {
                include snippets/fastcgi-php.conf;
                fastcgi_pass unix:/var/run/php/php7.2-fpm.sock;
        }

        location ^~ /php/ {
            return 404;
        }
}
```
> **Note:** This is just a simple `nginx` configuration. A running configuration will be more complex with SSL certificates and so on.

Be sure to setup your own SSL certificate using certbot.
```sh
certbot -d <your-domain.com>
```